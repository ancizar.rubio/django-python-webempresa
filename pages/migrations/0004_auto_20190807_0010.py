# Generated by Django 2.2.3 on 2019-08-07 05:10

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20190807_0006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='content',
            field=ckeditor.fields.RichTextField(default=0, verbose_name='Contenido'),
        ),
        migrations.AlterField(
            model_name='page',
            name='order',
            field=models.SmallIntegerField(verbose_name='Orden'),
        ),
    ]
