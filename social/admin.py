from django.contrib import admin
from .models import Link

# Register your models here.
class LinkAdmin(admin.ModelAdmin):
    readonly_fields = ('Created', 'Updated')

    def get_readonly_fields(self,request,obj=None):
        if request.user.groups.filter(name="personal").exists():
            return('Key', 'name')
        else:
            return('Created', 'Updated')

admin.site.register(Link, LinkAdmin)